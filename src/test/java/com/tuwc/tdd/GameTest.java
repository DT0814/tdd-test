package com.tuwc.tdd;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class GameTest {

    @Test
    public void should_be_return_0() {
        Bowling bowling = new Bowling();
        Assertions.assertEquals(0, bowling.getScore());
    }

    @Test
    void should_be_return_one_throw_score() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(6));
        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(6, bowling.getScore());
    }

    @Test
    void test_10_without_strike_and_spare() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(6, 1));
        frames.add(new Frame(6, 2));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(2, 2));
        frames.add(new Frame(4, 4));
        frames.add(new Frame(0, 3));
        frames.add(new Frame(4, 1));
        frames.add(new Frame(0, 9));
        frames.add(new Frame(1, 8));
        frames.add(new Frame(9, 0));
        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(71, bowling.getScore());
    }

    @Test
    void test_first_frames_is_spare() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(6, 4));
        frames.add(new Frame(6, 2));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(2, 2));
        frames.add(new Frame(4, 4));
        frames.add(new Frame(0, 3));
        frames.add(new Frame(4, 1));
        frames.add(new Frame(0, 9));
        frames.add(new Frame(1, 8));
        frames.add(new Frame(9, 0));
        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(80, bowling.getScore());
    }

    @Test
    void Given_10_frames_with_spare_without_strike() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(4, 4));
        frames.add(new Frame(6, 2));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(2, 2));
        frames.add(new Frame(4, 4));
        frames.add(new Frame(0, 3));
        frames.add(new Frame(4, 1));
        frames.add(new Frame(1, 9));
        frames.add(new Frame(1, 8));
        frames.add(new Frame(9, 0));
        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(74, bowling.getScore());
    }

    @Test
    void Given_10_frames_and_the_last_frame_is_spare_and_there_is_no_strike() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(4, 4));
        frames.add(new Frame(6, 2));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(2, 2));
        frames.add(new Frame(4, 4));
        frames.add(new Frame(0, 3));
        frames.add(new Frame(4, 1));
        frames.add(new Frame(0, 9));
        frames.add(new Frame(1, 8));
        frames.add(new Frame(9, 1));
        frames.add(new Frame(8));
        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(81, bowling.getScore());
    }

    @Test
    void Given_only_first_frame_is_strike() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(10));
        frames.add(new Frame(6, 2));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(2, 2));
        frames.add(new Frame(4, 4));
        frames.add(new Frame(0, 3));
        frames.add(new Frame(4, 1));
        frames.add(new Frame(0, 9));
        frames.add(new Frame(1, 8));
        frames.add(new Frame(9, 0));
        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(82, bowling.getScore());
    }

    @Test
    void Given_10_frames_with_strike_and_the_last_frame_is_not_strike() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(6, 1));
        frames.add(new Frame(6, 2));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(2, 2));
        frames.add(new Frame(4, 4));
        frames.add(new Frame(10));
        frames.add(new Frame(4, 1));
        frames.add(new Frame(0, 9));
        frames.add(new Frame(1, 8));
        frames.add(new Frame(9, 0));
        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(71 + 7 + 5, bowling.getScore());
    }

    @Test
    void Given_10_frames_and_the_last_frame_is_strike() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(6, 1));
        frames.add(new Frame(6, 2));
        frames.add(new Frame(3, 6));
        frames.add(new Frame(2, 2));
        frames.add(new Frame(4, 4));
        frames.add(new Frame(0, 3));
        frames.add(new Frame(4, 1));
        frames.add(new Frame(0, 9));
        frames.add(new Frame(1, 8));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));

        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(92, bowling.getScore());
    }

    @Test
    void Given_a_perfect_game_with_10_strikes_when_I_get_score_then_total_score_should_be_300() {
        List<Frame> frames = new ArrayList<>();
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        frames.add(new Frame(10));
        Bowling bowling = new Bowling(frames);
        bowling.calculate();
        Assertions.assertEquals(300, bowling.getScore());
    }

    @Test
    void given_a_empty_list_when_throw_error() {
        List<Frame> frames = new ArrayList<>();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Bowling(frames);
        });
    }

    @Test
    void given_frame_a_illegal_params_when_throw_error() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Frame frame = new Frame(10, 2);
        });
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Frame frame1 = new Frame(-1, 0);
        });
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Frame frame2 = new Frame(2, 10);
        });
    }
}
